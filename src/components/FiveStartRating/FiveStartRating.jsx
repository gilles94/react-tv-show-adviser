import s from "./style.module.css"
import {StarFill,Star as StarEmpty , StarHalf} from "react-bootstrap-icons";
export function FiveStartRating({rating}){
    const startList=[];
    const StarFillCount=Math.floor(rating);
    const hasStarHalf=rating-StarFillCount>=0.5;
    const emptyStarCount=5-StarFillCount-(hasStarHalf?1:0);
    console.log(StarFillCount,hasStarHalf,emptyStarCount);
    for(let i=1;i<=StarFillCount;i++){
        startList.push(<StarFill key={"star-fill"+i}/>)
    }if(hasStarHalf){
        startList.push(<StarHalf key={"star-half"}/>)
    }
    for(let i=1;i<=emptyStarCount;i++){
        startList.push(<StarEmpty key={"star-empty"+i}/>)
    }
    
    return <div>
        
        {startList}
    </div>
}