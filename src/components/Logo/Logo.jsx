import s from "./style.module.css";
export function Logo({image,title,subtitle}){
    return <>
    <div className={s.container}>
        <img src={image} className={s.image}/>
        <span className={s.title}>{subtitle}</span>
    </div>
    <span className={s.subtitle}>{subtitle}</span>
    </> ;
}