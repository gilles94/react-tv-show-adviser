import { useState } from "react";
import { TVShowAPI } from "./api/tv-show";
import "./global.css";
import logo from "./assets/images/logo.png";
import s from "./style.module.css";
import { useEffect } from "react";
import { BACKDROP_BASE_URL } from "./config";
import { TVShowDetail } from "./components/TVShowDetail/TVShowDetail";
import { Logo } from "./components/Logo/Logo";
import { TVShowListItem } from "./components/TVShowListItem/TVShowListItem";
import { TVShowList } from "./components/TVShowList/TVShowList";
import { SearchBar } from "./components/SearchBar/SearchBar";
export function App() {
  const [currentTVShow, setCurrentTVShow] = useState();
  const [recommendationList, setRecommendationList] = useState([]);

  async function fetchPopulars() {
   try {
    const populate = await TVShowAPI.fetchPopulars();
    if (populate.length > 0) {
      setCurrentTVShow(populate[0]);
    }
   }catch(error){
     alert("Erreur durant la recherche des series populaires")
   }
  
  }

  async function fetchRecommendations(tvShowId) {
    const recommentations = await TVShowAPI.fetchRecommendations(tvShowId);
    if (recommentations.length > 0) {
      setRecommendationList(recommentations.slice(0, 10));
    }
  }
  useEffect(() => {
    fetchPopulars();
  }, []);

  useEffect(() => {
    if (currentTVShow) {
      fetchRecommendations(currentTVShow.id);
    }
  }, [currentTVShow]);
  async function searchTvShow(tvShowName) {
    const searchReponse = await TVShowAPI.fetchByTitle(tvShowName);
    if (searchReponse.length > 0) {
      setCurrentTVShow(searchReponse[0]);
    }

  }

  return (
    <div
      className={s.main_container}
      style={{
        background: currentTVShow
          ? `linear-gradient(rgba(0,0,0,0.55), rgba(0,0,0,0.55)), url("${BACKDROP_BASE_URL}${currentTVShow.backdrop_path}") no-repeat center / cover`
          : "black",
      }}
    >
      <div className={s.header}>
        <div className="row">
          <div className="col-4">
            <Logo
              image={logo}
              title="Watowatch"
              subtitle="Find a show you may like"
            />
          </div>
          <div className="col-sm-12 col-lg-4">
            <SearchBar onSubmit={searchTvShow} />
          </div>
        </div>
      </div>
      <div className={s.tv_show_detail}>
        {currentTVShow && <TVShowDetail tvShow={currentTVShow} />}
      </div>
      <div className={s.recommentations}>
        {recommendationList && (
          <>
            <TVShowList
              onClickItem={setCurrentTVShow}
              tvShowList={recommendationList}
            />
          </>
        )}
      </div>
    </div>
  );
}
