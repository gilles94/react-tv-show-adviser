const BASE_URL = "https://api.themoviedb.org/3/";
const API_KEY_PARAM = "?api_key=369554f35579d35cfecdf116f5e2edfa";
const BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/original/";
const SMALL_IMG_COVER_BASE_URL = "https://image.tmdb.org/t/p/w300/";
export { SMALL_IMG_COVER_BASE_URL, BASE_URL, API_KEY_PARAM, BACKDROP_BASE_URL };
